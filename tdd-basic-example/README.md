# TDD Basic Example

This project shows a basic TDD example with some of the most common annotations.

This project used Java and Maven.

We have to declare the maven-surefire-plugin (version 2.19.1) in our pom.xml file and configure the dependencies of this plugin. We have to declare the following dependencies:

The junit-platform-surefire-provider (version 1.1.0) dependency allows us to run tests that use either the “old” JUnit (3 or 4) or JUnit 5.

If we want to run tests that use either JUnit 3 or 4, we have to declare the junit-vintage-engine (version 5.1.0) dependency.

If we want to run tests that use JUnit 5, we have to declare the junit-jupiter-engine (version 5.1.0) dependency.

* Junit Jupiter Dependency graph
  - https://junit.org/junit5/docs/current/user-guide/
* JUnit Annotation
  - https://junit.org/junit5/docs/current/user-guide/#writing-tests-annotations

* Maven goals
  - clean test
    - run the unit tests
  - javadoc:javadoc
    - generate javadoc for the code
  - javadoc:test-javadoc
    - generate javadoc for the test cases
  - test jacoco:report
    - generate jacoco coverage report
  - test jacoco:check
    - check if thresholds limits are achieved
  - org.pitest:pitest-maven:mutationCoverage
    - generates a PIT Mutation coverage report to target/pit-reports/YYYYMMDDHHMI
  - org.pitest:pitest-maven:mutationCoverage -DwithHistory
      - generates a quicker PIT Mutation coverage report to target/pit-reports/YYYYMMDDHHMI

## Jacoco dependencies
* https://github.com/pitest/pitest-junit5-plugin
  - https://mvnrepository.com/artifact/org.pitest/pitest-junit5-plugin
    - required to work with JUnit5
